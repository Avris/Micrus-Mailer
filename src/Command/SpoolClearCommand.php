<?php
namespace Avris\Micrus\Mailer\Command;

use Avris\Micrus\Mailer\Mailer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Lock\Factory;
use Symfony\Component\Lock\Store\SemaphoreStore;

final class SpoolClearCommand extends Command
{
    /** @var Mailer */
    private $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
        parent::__construct();
    }

    public function configure()
    {
        $this
            ->setName('mailer:spool:clear')
            ->addOption('force', 'f')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lock = (new Factory(new SemaphoreStore()))->createLock('mailer:spool');
        if (!$lock->acquire()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        $question = new ConfirmationQuestion('Are you sure you want to clear the mailing spool? (y/N) ', false);

        if (!$input->getOption('force') && !$helper->ask($input, $output, $question)) {
            return 0;
        }

        $this->mailer->clearSpool();

        $output->writeln('Spool cleared');

        $lock->release();
    }
}
