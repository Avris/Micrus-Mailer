<?php
namespace Avris\Micrus\Mailer\Command;

use Avris\Micrus\Mailer\Mail\Mail;
use Avris\Micrus\Mailer\Mailer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\LockHandler;
use Symfony\Component\Lock\Factory;
use Symfony\Component\Lock\Store\SemaphoreStore;

final class SpoolSendCommand extends Command
{
    /** @var Mailer */
    private $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
        parent::__construct();
    }

    public function configure()
    {
        $this
            ->setName('mailer:spool:send')
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'How many messages to send out at once', 100);
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lock = (new Factory(new SemaphoreStore()))->createLock('mailer:spool');
        if (!$lock->acquire()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        $i = 0;
        foreach ($this->mailer->sendOutSpool($input->getOption('limit')) as $status) {
            switch ($status->getType()) {
                case SpoolStatus::TYPE_START:
                    $output->write(sprintf('[%s] ', ++$i));
                    break;
                case SpoolStatus::TYPE_SUCCESS:
                    $output->writeln(sprintf(
                        'Sent "%s" to %s',
                        $status->getPayload()->getSubject(),
                        $status->getPayload()->getRecipientsString()
                    ));
                    break;
                case SpoolStatus::TYPE_FAIL:
                    $payload = $status->getPayload();
                    if ($payload instanceof Mail) {
                        $output->writeln(sprintf('FAILED sending "%s" to %s',
                            $status->getPayload()->getSubject(),
                            $status->getPayload()->getRecipientsString()
                        ));
                    } elseif ($payload instanceof \Throwable) {
                        $output->writeln(sprintf('FAILED: %s: %s',
                            get_class($payload),
                            $payload->getMessage()
                        ));
                    } else {
                        $output->writeln('FAILED');
                    }
                    break;
            }
        }

        $lock->release();
    }
}
