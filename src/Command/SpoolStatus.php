<?php
namespace Avris\Micrus\Mailer\Command;

use Avris\Micrus\Mailer\Mail\Mail;

final class SpoolStatus
{
    const TYPE_START = 1;
    const TYPE_SUCCESS = 2;
    const TYPE_FAIL = 3;

    /** @var int */
    private $type;

    /** @var int|Mail */
    private $payload;

    public function __construct(int $type, $payload = null)
    {
        $this->type = $type;
        $this->payload = $payload;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getPayload()
    {
        return $this->payload;
    }
}
