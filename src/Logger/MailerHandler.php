<?php
namespace Avris\Micrus\Mailer\Logger;

use Avris\Bag\Bag;
use Avris\Micrus\Mailer\Mail\Address;
use Avris\Micrus\Mailer\Mail\Mail;
use Avris\Micrus\Mailer\Mailer;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\MailHandler;
use Monolog\Logger;

final class MailerHandler extends MailHandler
{
    /** @var Mailer */
    private $mailer;

    /** @var Bag */
    private $config;

    public function __construct(
        Mailer $mailer,
        Bag $configMailer_logger,
        string $level = Logger::DEBUG,
        bool $bubble = true
    ) {
        $this->mailer = $mailer;
        $this->config = $configMailer_logger;
        parent::__construct($level, $bubble);
    }

    protected function send($content, array $records)
    {
        if (empty($this->config->get('admins'))) {
            return;
        }

        $mail = new Mail();

        foreach ($this->config->get('admins') as $email => $name) {
            $mail->addTo(new Address($email, $name));
        }

        if ($records) {
            $subjectFormatter = new LineFormatter($this->config->get('subject'));
            $mail->setSubject($subjectFormatter->format($this->getHighestRecord($records)));
        }

        $mail->setBody($content);

        $this->mailer->send($mail);
    }
}
