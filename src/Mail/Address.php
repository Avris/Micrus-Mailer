<?php
namespace Avris\Micrus\Mailer\Mail;

final class Address implements AddressInterface
{
    /** @var string */
    private $email;

    /** @var string */
    private $name;

    public function __construct(string $email, string $name = '')
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException(sprintf('"%s" is not a valid email', $email));
        }

        $this->email = $email;
        $this->name = $name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function __toString(): string
    {
        return $this->name ?: $this->email;
    }
}
