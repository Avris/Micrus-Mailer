<?php
namespace Avris\Micrus\Mailer\Mail;

interface AddressInterface
{
    /**
     * @return string
     */
    public function getEmail();

    /**
     * @return string
     */
    public function getName();
}
