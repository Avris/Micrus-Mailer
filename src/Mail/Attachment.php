<?php
namespace Avris\Micrus\Mailer\Mail;

use Avris\Micrus\Exception\InvalidArgumentException;

final class Attachment
{
    /** @var string */
    private $path;

    /** @var string */
    private $filename;

    /** @var string */
    private $mimeType;

    public function __construct(string $path, string $filename = '')
    {
        if (!file_exists($path)) {
            throw new InvalidArgumentException(sprintf('File "%s" does not exist', $path));
        }

        $this->path = $path;

        $this->filename = $filename ?: basename($path);

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $this->mimeType = finfo_file($finfo, $path);
        finfo_close($finfo);
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getMimeType(): string
    {
        return $this->mimeType;
    }
}
