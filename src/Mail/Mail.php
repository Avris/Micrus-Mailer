<?php
namespace Avris\Micrus\Mailer\Mail;

final class Mail
{
    /** @var AddressInterface */
    private $from;

    /** @var string */
    private $subject;

    /** @var string */
    private $body;

    /** @var string */
    private $altBody;

    /** @var AddressInterface[] */
    private $to = [];

    /** @var AddressInterface[] */
    private $cc = [];

    /** @var AddressInterface[] */
    private $bcc = [];

    /** @var AddressInterface[] */
    private $replyTo = [];

    /** @var Attachment[] */
    private $attachments = [];

    /** @var Attachment[] */
    private $embeddedImages = [];

    public function getFrom(): ?AddressInterface
    {
        return $this->from;
    }

    public function setFrom(AddressInterface $from): self
    {
        $this->from = $from;

        return $this;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;
        return $this;
    }

    public function getAltBody(): ?string
    {
        return $this->altBody;
    }

    public function setAltBody(string $altBody): self
    {
        $this->altBody = $altBody;

        return $this;
    }

    public function addTo(AddressInterface $address): self
    {
        $this->to[] = $address;

        return $this;
    }

    /**
     * @return AddressInterface[]
     */
    public function getTo(): array
    {
        return $this->to;
    }

    public function addCc(AddressInterface $address): self
    {
        $this->cc[] = $address;

        return $this;
    }

    /**
     * @return AddressInterface[]
     */
    public function getCc(): array
    {
        return $this->cc;
    }

    public function addBcc(AddressInterface $address): self
    {
        $this->bcc[] = $address;

        return $this;
    }

    /**
     * @return AddressInterface[]
     */
    public function getBcc(): array
    {
        return $this->bcc;
    }

    public function addReplyTo(AddressInterface $address): self
    {
        $this->replyTo[] = $address;

        return $this;
    }

    /**
     * @return AddressInterface[]
     */
    public function getReplyTo(): array
    {
        return $this->replyTo;
    }

    public function addAttachment(string $path, string $filename = ''): self
    {
        $this->attachments[] = new Attachment($path, $filename);

        return $this;
    }

    public function embedImage(string $cid, string $path, string $filename = ''): self
    {
        $this->embeddedImages[$cid] = new Attachment($path, $filename);
        return $this;
    }

    /**
     * @return Attachment[]
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    /**
     * @return Attachment[]
     */
    public function getEmbeddedImages(): array
    {
        return $this->embeddedImages;
    }

    public function getRecipientsString(): string
    {
        $out = [];

        foreach ($this->getTo() as $address) {
            $out[] = $this->addressToString($address);
        }

        foreach ($this->getCc() as $address) {
            $out[] = 'CC:' . $this->addressToString($address);
        }

        foreach ($this->getBcc() as $address) {
            $out[] = 'BCC:' . $this->addressToString($address);
        }

        return join(', ', $out);
    }

    private function addressToString(AddressInterface $address)
    {
        return $address->getName()
            ? sprintf('%s <%s>', $address->getName(), $address->getEmail())
            : $address->getEmail();
    }

    public function __toString(): string
    {
        return preg_replace_callback(
            '#["\']cid\:([A-Za-z0-9-_]+)["\']#',
            function ($matches) {
                $cid = $matches[1];
                if (!isset($this->embeddedImages[$cid])) {
                    return $matches[0];
                }

                $attachment = $this->embeddedImages[$cid];

                return sprintf(
                    'data:%s;base64,%s',
                    $attachment->getMimeType(),
                    base64_encode(file_get_contents($attachment->getPath()))
                );
            },
            $this->getBody()
        );
    }
}
