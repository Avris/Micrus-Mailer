<?php
namespace Avris\Micrus\Mailer;

use Avris\Localisator\LocalisatorInterface;
use Avris\Localisator\Order\LocaleOrderProviderInterface;
use Avris\Micrus\Exception\NotFoundException;
use Avris\Micrus\Mailer\Mail\Address;
use Avris\Micrus\Mailer\Mail\Mail;
use Avris\Bag\Bag;
use Avris\Micrus\View\AssetProvider;
use Avris\Micrus\View\Templater;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

class MailBuilder
{
    /** @var Bag */
    private $config;

    /** @var LocalisatorInterface */
    private $localizator;

    /** @var Templater */
    private $templater;

    /** @var string */
    private $css;

    /** @var CssToInlineStyles */
    private $styleInliner;

    /** @var LocaleOrderProviderInterface */
    private $orderProvider;

    public function __construct(
        Bag $configMailer,
        LocalisatorInterface $localizator,
        Templater $templater,
        Bag $configAssets,
        AssetProvider $assetProvider,
        CssToInlineStyles $styleInliner,
        LocaleOrderProviderInterface $orderProvider
    ) {
        $this->config = $configMailer;
        $this->localizator = $localizator;
        $this->templater = $templater;
        $this->css = $assetProvider->read($configAssets->get('mailCss'));
        $this->styleInliner = $styleInliner;
        $this->orderProvider = $orderProvider;
    }

    public function build(MailTemplateInterface $template, string $locale = null, array $vars = [])
    {
        if ($locale) {
            $this->orderProvider->rebuild($locale);
        }

        $mail = new Mail();

        if ($this->config->has('from')) {
            $mail->setFrom(new Address($this->config->get('from'), $this->config->get('fromName')));
        }

        $mail->setSubject($this->localizator->get('mail:' . $template->getName() . '.subject', $vars));

        $body = $this->buildBody($template->getName(), 'html', 'body', $vars);
        $mail->setBody(
            $this->styleInliner ? $this->styleInliner->convert($body, $this->css) : $body
        );

        $altBody = $this->buildBody($template->getName(), 'text', 'altBody', $vars)
            ?: preg_replace('#^\s+#m', '', strip_tags($body));
        $mail->setAltBody($altBody);

        try {
            return $template->extend($mail);
        } finally {
            if ($locale) {
                $this->orderProvider->rebuild();
            }
        }
    }

    private function buildBody(string $templateName, string $type, string $key, array $vars): ?string
    {
        try {
            return $this->templater->render(
                sprintf('Mail/%s.%s.twig', $templateName, $type),
                $vars
            );
        } catch (NotFoundException $e) {
            return $this->localizator->get(
                sprintf('mail:%s.%s', $templateName, $key),
                $vars
            );
        }
    }
}
