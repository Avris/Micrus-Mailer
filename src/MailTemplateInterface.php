<?php
namespace Avris\Micrus\Mailer;

use Avris\Micrus\Mailer\Mail\Mail;

interface MailTemplateInterface
{
    public function getName(): string;

    public function extend(Mail $mail): Mail;
}
