<?php
namespace Avris\Micrus\Mailer;

use Avris\Micrus\Exception\ConfigException;
use Avris\Micrus\Mailer\Mail\Mail;
use Avris\Micrus\Mailer\Sender\SenderInterface;
use Avris\Micrus\Mailer\Command\SpoolStatus;
use Avris\Micrus\Mailer\Spool\SpoolInterface;

class Mailer
{
    /** @var SenderInterface */
    private $sender;

    /** @var SpoolInterface */
    private $spool;

    public function __construct(
        SenderInterface $sender,
        SpoolInterface $spool
    ) {
        $this->sender = $sender;
        $this->spool = $spool;
    }

    public function send(Mail $mail)
    {
        return $this->sender->send($mail);
    }

    public function spool(Mail $mail)
    {
        return $this->spool->save($mail);
    }

    /**
     * @return SpoolStatus[]|\Generator
     */
    public function sendOutSpool(int $limit = 100)
    {
        foreach ($this->spool->fetch($limit) as $key => $mail) {
            yield new SpoolStatus(SpoolStatus::TYPE_START, $mail);
            try {
                $result = $this->send($mail);
            } catch (\Throwable $e) {
                yield new SpoolStatus(SpoolStatus::TYPE_FAIL, $e);
                continue;
            }
            $this->spool->remove($mail, $key);
            yield new SpoolStatus($result ? SpoolStatus::TYPE_SUCCESS : SpoolStatus::TYPE_FAIL, $mail);
        }
    }

    public function clearSpool()
    {
        $this->spool->clear();
    }
}
