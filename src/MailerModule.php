<?php
namespace Avris\Micrus\Mailer;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;
use Avris\Micrus\Tool\Config\ParametersProvider;

class MailerModule implements ModuleInterface, ParametersProvider
{
    use ModuleTrait;

    public function getParametersDefaults(): array
    {
        return [
            'MAILER_URL' => 'null://localhost',
        ];
    }
}
