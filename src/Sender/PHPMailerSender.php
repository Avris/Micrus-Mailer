<?php
namespace Avris\Micrus\Mailer\Sender;

use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Mailer\Mail\Mail;
use Avris\Micrus\Mailer\Transport;
use PHPMailer\PHPMailer\PHPMailer;

class PHPMailerSender implements SenderInterface
{
    /** @var PHPMailer|null */
    protected $prefab;

    public function __construct(Transport $transport)
    {
        $this->prefab = new PHPMailer(true);
        switch ($transport->getScheme()) {
            case 'null':
                $this->prefab = null;
                return;
            case 'mail':
                $this->prefab->isMail();
                break;
            case 'smtp':
                $this->prefab->isSMTP();
                break;
            case 'qmail':
                $this->prefab->isQmail();
                break;
            case 'sendmail':
                $this->prefab->isSendmail();
                break;
            default:
                throw new InvalidArgumentException(sprintf('Unsupported scheme: %s', $transport->getScheme()));
        }

        $this->prefab->Host = $transport->getHost();
        $this->prefab->Port = $transport->getPort();

        if ($transport->getEncryption()) {
            $this->prefab->SMTPSecure = $transport->getEncryption();
        }

        if ($transport->getAuthMode() === 'login') {
            $this->prefab->SMTPAuth = true;
            $this->prefab->Username = $transport->getUsername();
            $this->prefab->Password = $transport->getPassword();
        }

        $this->prefab->CharSet = 'utf-8';
        $this->prefab->isHTML(true);
        $this->prefab->WordWrap = 78;
    }

    public function send(Mail $mail)
    {
        if (!$this->prefab) {
            return true;
        }

        $phpMailer = clone $this->prefab;

        if ($mail->getFrom()) {
            $phpMailer->From = $mail->getFrom()->getEmail();
            $phpMailer->FromName = $mail->getFrom()->getName();
        }

        foreach ($mail->getTo() as $address) {
            $phpMailer->addAddress($address->getEmail(), $address->getName());
        }

        foreach ($mail->getCc() as $address) {
            $phpMailer->addCC($address->getEmail(), $address->getName());
        }

        foreach ($mail->getBcc() as $address) {
            $phpMailer->addBCC($address->getEmail(), $address->getName());
        }

        foreach ($mail->getReplyTo() as $address) {
            $phpMailer->addReplyTo($address->getEmail(), $address->getName());
        }

        $phpMailer->Subject = $mail->getSubject();
        $phpMailer->Body = $mail->getBody();
        $phpMailer->AltBody = $mail->getAltBody();

        foreach ($mail->getAttachments() as $attachment) {
            $phpMailer->addAttachment(
                $attachment->getPath(),
                $attachment->getFilename(),
                'base64',
                $attachment->getMimeType()
            );
        }

        foreach ($mail->getEmbeddedImages() as $cid => $attachment) {
            $phpMailer->addEmbeddedImage(
                $attachment->getPath(),
                $cid,
                $attachment->getFilename(),
                'base64',
                $attachment->getMimeType()
            );
        }

        return $phpMailer->send();
    }
}
