<?php
namespace Avris\Micrus\Mailer\Sender;

use Avris\Micrus\Mailer\Mail\Mail;

interface SenderInterface
{
    /**
     * @param Mail $mail
     * @return bool
     */
    public function send(Mail $mail);
}
