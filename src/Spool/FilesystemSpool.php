<?php
namespace Avris\Micrus\Mailer\Spool;

use Avris\Micrus\Mailer\Mail\Mail;
use Avris\Micrus\Tool\DirectoryCleaner;

class FilesystemSpool implements SpoolInterface
{
    /** @var string */
    private $dir;

    /** @var DirectoryCleaner */
    private $directoryCleaner;

    public function __construct(string $dir, DirectoryCleaner $directoryCleaner)
    {
        $this->dir = $dir;
        $this->directoryCleaner = $directoryCleaner;
    }

    public function save(Mail $mail): bool
    {
        if (!is_dir($this->dir)) {
            if (!@mkdir($this->dir, 0777, true)) {
                throw new \RuntimeException('Cannot write to spool in ' . $this->dir);
            }
        }

        return (bool) file_put_contents(
            sprintf(
                '%s/%s_%s.mail.obj',
                $this->dir,
                (new \DateTime())->format('YmdHis'),
                substr(hash('sha256', mt_rand()), 0, 10)
            ),
            serialize($mail)
        );
    }

    public function fetch(int $limit = 100): \Generator
    {
        foreach (array_slice(glob($this->dir . '/*.mail.obj'), 0, $limit) as $file) {
            $mail = @unserialize(file_get_contents($file));
            if (!$mail instanceof Mail) {
                throw new \RuntimeException(sprintf('Failed reading file "%s" as a Mail object', $file));
            }
            yield $file => $mail;
        }
    }

    public function remove(Mail $mail, $key)
    {
        return (bool) unlink($key);
    }

    public function clear()
    {
        $this->directoryCleaner->removeDir($this->dir);
    }
}
