<?php
namespace Avris\Micrus\Mailer\Spool;

use Avris\Micrus\Bootstrap\Terminator;
use Avris\Micrus\Mailer\Mail\Mail;
use Avris\Micrus\Mailer\Sender\SenderInterface;

class MemorySpool implements SpoolInterface
{
    /** @var Terminator */
    private $terminator;

    /** @var SenderInterface */
    private $sender;

    public function __construct(Terminator $terminator, SenderInterface $sender)
    {
        $this->terminator = $terminator;
        $this->sender = $sender;
    }

    public function save(Mail $mail): bool
    {
        $this->terminator->attach(function () use ($mail) {
            $this->sender->send($mail);
        });

        return true;
    }

    public function fetch(int $limit = 100): \Generator
    {
        yield from [];
    }

    public function remove(Mail $mail, $key)
    {
        return false;
    }

    public function clear()
    {
        $this->callables = [];
    }
}
