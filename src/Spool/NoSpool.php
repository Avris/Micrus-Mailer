<?php
namespace Avris\Micrus\Mailer\Spool;

use Avris\Micrus\Mailer\Mail\Mail;
use Avris\Micrus\Mailer\Sender\SenderInterface;

class NoSpool implements SpoolInterface
{
    /** @var SenderInterface */
    private $sender;

    public function __construct(SenderInterface $sender)
    {
        $this->sender = $sender;
    }

    public function save(Mail $mail): bool
    {
        return $this->sender->send($mail);
    }

    public function fetch(int $limit = 100): \Generator
    {
        yield from [];
    }

    public function remove(Mail $mail, $key)
    {
        return false;
    }

    public function clear()
    {
    }
}
