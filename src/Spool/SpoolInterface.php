<?php
namespace Avris\Micrus\Mailer\Spool;

use Avris\Micrus\Mailer\Mail\Mail;

interface SpoolInterface
{
    public function save(Mail $mail): bool;

    /**
     * @return Mail[]|\Generator
     */
    public function fetch(int $limit = 100): \Generator;

    public function remove(Mail $mail, $key);

    public function clear();
}
