<?php
namespace Avris\Micrus\Mailer;

final class Transport
{
    /** @var string */
    private $scheme;

    /** @var string|null */
    private $username;

    /** @var string|null */
    private $password;

    /** @var string|null */
    private $host;

    /** @var int */
    private $port;

    /** @var string|null */
    private $encryption;

    /** @var int|null */
    private $timeout;

    /** @var string|null */
    private $sourceIp;

    /** @var string|null */
    private $localDomain;

    /** @var string|null */
    private $authMode;

    /** @var string|null */
    private $command;

    public function __construct(string $envMailerUrl)
    {
        $parts = parse_url($envMailerUrl);

        $this->scheme = $parts['scheme'] ?? 'null';
        $this->username = $parts['user'] ?? null;
        $this->password = $parts['pass'] ?? null;
        $this->host = $parts['host'] ?? null;
        $this->port = $parts['port'] ?? null;

        parse_str($parts['query'] ?? '', $query);
        $this->encryption = $query['encryption'] ?? null;
        $this->timeout = $query['timeout'] ?? null;
        $this->sourceIp = $query['source_ip'] ?? null;
        $this->localDomain = $query['local_domain'] ?? null;
        $this->authMode = $query['auth_mode'] ?? null;
        $this->command = $query['command'] ?? null;

        if ($this->scheme === 'gmail') {
            $this->scheme = 'smtp';
            $this->host = 'smtp.gmail.com';
            $this->encryption = 'ssl';
            $this->authMode = 'login';
        }

        if (!$this->port) {
            $this->port = $this->encryption === 'ssl' ? 465 : 25;
        }
    }

    public function getScheme(): string
    {
        return $this->scheme;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function getPort(): int
    {
        return $this->port;
    }

    public function getEncryption(): ?string
    {
        return $this->encryption;
    }

    public function getTimeout(): ?int
    {
        return $this->timeout === null ? null : (int) $this->timeout;
    }

    public function getSourceIp(): ?string
    {
        return $this->sourceIp;
    }

    public function getLocalDomain(): ?string
    {
        return $this->localDomain;
    }

    public function getAuthMode(): ?string
    {
        return $this->authMode;
    }

    public function getCommand(): ?string
    {
        return $this->command;
    }
}
